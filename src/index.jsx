import { App } from "./components/App";

const initialState = { utcDateTime: null };
const appReducer = (state = initialState, action) => {
  if (action.type === "utcDateTimeFetched") {
    return {
      ...state,
      utcDateTime: action.payload,
    };
  }
  return state;
};
const store = Redux.createStore(
  appReducer,
  Redux.applyMiddleware(ReduxThunk.default)
);
ReactDOM.render(
  <ReactRedux.Provider store={store}>
    <App />
  </ReactRedux.Provider>,
  document.getElementById("root")
);
