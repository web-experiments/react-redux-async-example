const actions = {
  fetchUtcDateTime: (payload) => {
    return { type: "utcDateTimeFetched", payload };
  },
};
export const fetchUtcDateTime = () => {
  return async (dispatch) => {
    const url = "http://worldclockapi.com/api/json/utc/now";
    const response = await fetch(url);
    const responseData = await response.json();
    const utcDateTime = new Date(
      responseData.currentFileTime / 10000 - 11644473600000
    );
    dispatch(actions.fetchUtcDateTime(utcDateTime));
  };
};
