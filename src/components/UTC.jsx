import { fetchUtcDateTime } from "../slice";

const UTC = ({ onTick, utcDateTime }) => {
  let timerId;
  React.useEffect(() => {
    timerId = setInterval(onTick, 1000);
    return () => {
      clearInterval(timerId);
    };
  }, []);
  let utcTime = utcDateTime?.toLocaleTimeString();
  utcTime = utcTime ?? "...";
  return <h3>UTC time is {utcTime}</h3>;
};
const mapDispatchToProps = (dispatch) => {
  return {
    onTick: () => dispatch(fetchUtcDateTime()),
  };
};
const mapStateToProps = (state) => {
  return {
    utcDateTime: state.utcDateTime,
  };
};
export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(UTC);
