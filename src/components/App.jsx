import BarbadosTime from "./BarbadosTime";
import JapanTime from "./JapanTime";
import UTC from "./UTC";

export const App = () => {
  const [showTime, setShowTime] = React.useState(false);
  const Welcome = () => <h1>Welcome</h1>;
  if (showTime) {
    return (
      <>
        <Welcome />
        <button onClick={() => setShowTime(false)}>Hide time</button>
        <UTC />
        <JapanTime />
        <BarbadosTime />
      </>
    );
  }
  return (
    <>
      <Welcome />
      <button onClick={() => setShowTime(true)}>Show time</button>
    </>
  );
};
