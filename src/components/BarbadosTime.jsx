const BarbadosTime = ({ utcDateTime }) => {
  const diff = -4;
  const country = "Barbados";
  const barbadosDateTime = new Date(utcDateTime?.getTime());
  barbadosDateTime?.setHours(barbadosDateTime?.getHours() + diff);
  const barbadosTime = barbadosDateTime?.toLocaleTimeString();
  return (
    <h3>
      The time in {country} is{" "}
      {barbadosTime === "Invalid Date" ? "..." : barbadosTime}
    </h3>
  );
};
const mapStateToProps = (state) => {
  return {
    utcDateTime: state.utcDateTime,
  };
};
export default ReactRedux.connect(mapStateToProps)(BarbadosTime);
