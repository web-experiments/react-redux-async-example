const JapanTime = ({ utcDateTime }) => {
  const diff = 9;
  const country = "Japan";
  const japanDateTime = new Date(utcDateTime?.getTime());
  japanDateTime?.setHours(japanDateTime?.getHours() + diff);
  const japanTime = japanDateTime?.toLocaleTimeString();
  return (
    <h3>
      The time in {country} is{" "}
      {japanTime === "Invalid Date" ? "..." : japanTime}
    </h3>
  );
};
const mapStateToProps = (state) => {
  return {
    utcDateTime: state.utcDateTime,
  };
};
export default ReactRedux.connect(mapStateToProps)(JapanTime);
