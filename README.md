## Run me (\*nix)

- `git clone https://gitlab.com/web-experiments/react-redux-async-example.git`
- `cd /path/to/react-redux-async-example`
- `python -m http.server` or `python -m SimpleHTTPServer`
- Navigate to `http://localhost:8000/`
